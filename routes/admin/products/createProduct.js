const express = require('express');
const router = express.Router();

const ProductController = require('../../../controllers/products')

router.post('/:category', ProductController.createProduct)
module.exports = router;