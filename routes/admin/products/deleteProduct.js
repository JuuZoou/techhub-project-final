const express = require('express');
const router = express.Router();

const ProductController = require("../../../controllers/products");

router.delete('/:category/:id', ProductController.deleteProduct),
module.exports = router;