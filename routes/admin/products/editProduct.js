const express = require('express');
const router = express.Router();

const ProductController = require("../../../controllers/products");

router.put('/:category/:id', ProductController.editProduct),
module.exports = router;