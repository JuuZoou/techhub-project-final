const express = require('express');
const router = express.Router();

const ProductController = require("../../../controllers/products");

router.get('/:category', ProductController.getProducts)
router.get('/', ProductController.getProducts)
module.exports = router;