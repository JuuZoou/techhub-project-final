const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const adminController = require("../../controllers/admin");


router.post('/', adminController.registration)
module.exports = router;