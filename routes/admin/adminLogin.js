const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const adminController = require("../../controllers/admin");
const fs = require('fs');

router.post('/', adminController.login),
module.exports = router;