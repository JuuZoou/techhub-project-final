const mongoose = require('mongoose');
const express = require('express');
const UsersController = require("../../controllers/users");
const router = express.Router();

router.post('/', UsersController.editUser)
module.exports = router;