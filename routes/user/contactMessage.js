const express = require('express');
const mongoose = require('mongoose');
const userMessages = require('../../models/userMessages')
const router = express.Router();
const UserControllers = require('../../controllers/users')

router.post('/', UserControllers.contactMessage)

module.exports = router;