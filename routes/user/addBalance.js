const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const UserControllers = require('../../controllers/users')

router.post('/', UserControllers.addBalance)

module.exports = router;