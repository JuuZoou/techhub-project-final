const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const UsersController = require("../../controllers/users");


router.post("/", UsersController.registration)
module.exports = router;