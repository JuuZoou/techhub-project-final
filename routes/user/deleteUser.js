const express = require('express');
const router = express.Router();

const UsersController = require("../../controllers/users");

router.delete('/:id', UsersController.deleteUser)
module.exports = router;