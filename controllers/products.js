const express = require("express");
const fs = require("fs");
const path = require("path");

module.exports = {
  createProduct: (req, res) => {
    console.log(req.body)
    const price=JSON.parse(req.body.price)
    const shipping = JSON.parse(req.body.shipping)
    let product = {
      name: req.body.name,
      price: price,
      id: req.body.id,
      size: req.body.size,
      color: req.body.color,
      weight: req.body.weight,
      description: req.body.description,
      shipping: shipping,
      brand: req.body.brand,
      category:req.body.category,
      src: req.body.src,
    };
    console.log(product)
    const products = JSON.parse(
        fs.readFileSync(path.join(__dirname, "../") + "/db.json")
    );

    console.log(products)
    products[req.params.category].push(product);
    
    fs.writeFileSync(path.join(__dirname, "../") + "/db.json", JSON.stringify(products));
    res.status(200).json({msg: 'success'})
  }, //end of creating product

  //Edit Product
  editProduct: async (req, res) => {
    const products = JSON.parse(
        fs.readFileSync(path.join(__dirname, "../") + "/db.json")
    );

    const product = products[req.params.category].find(prod => prod.id === req.params.id);
    // return res.status(200).json(product);
      
    if (!product) {
        return res.status(404).json({msg: 'Product not found'});
    } 
    product.name = req.body.name;
    product.price = req.body.price;
    product.size = req.body.size;
    product.color = req.body.color;
    product.weight = req.body.weight;
    product.description = req.body.description;
    product.price = req.body.price;
    product.shipping = req.body.shipping;
    product.brand = req.body.brand;
    return res.json(product);
  },
  //end of edit product
  deleteProduct: (req, res) => {
    const products = JSON.parse(
        fs.readFileSync(path.join(__dirname, "../") + "/db.json")
    );
    const index = products[req.params.category].findIndex(prod => prod['id'] === req.params.id);
    if(index == -1) return res.status(404).json({msg: 'product not found'});
    products[req.params.category].splice(index,1);
    res.status(200).json({msg: 'deleted successfully'});
    fs.writeFileSync(path.join(__dirname, "../") + "/db.json",JSON.stringify(products))
  },
  //end of delete products
  //product list
  getProducts: (req, res) => {
    const products = JSON.parse(
      fs.readFileSync(path.join(__dirname, "../") + "/db.json")
    );
    if (!products) return res.status(404).json({msg: 'No Products found'});
    if (!req.params.category) {
        return res.status(200).json(products);
    }
    res.status(200).json({ [req.params.category]: products[req.params.category] })
    
    //end of product list
  }
};
