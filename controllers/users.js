const express = require('express');
const mongoose = require('mongoose');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const  User  = require('../models/user');
const router = express.Router();

module.exports = {
    
    registration : async (req,res)=>{
        let email = await User.findOne({ email : req.body.email });
        if(email) return  res.status(400).send('user already exists');
        const userDatas = _.pick(req.body,['username'],['email'],['password'],['repeatpassword'],['age'],['balance']);
        const user = await new User(userDatas);
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        user.repeatpassword = await bcrypt.hash(user.repeatpassword, salt);
        await user.save();
        res.json(user); 
    },
    //end of registration

    login :  async (req,res)=>{
        let user = await User.findOne({email : req.body.email});
        console.log(user)
        if(!user) {
           return  res.status(553).send('user doesnot exists');
        } else {
            const validPassword = await bcrypt.compare(req.body.password, user.password);
            if (!validPassword) return  res.status(400).send('wrong password');
            const token = jwt.sign({user:user},'jwtPrivateKey');
            return res.json(token);
        }
    },
    //end og login

    editUser : async (req,res)=>{
        let user = await User.findOne({_id : req.body.id});
        if(user){

            let validPassword = await bcrypt.compare(req.body.oldpassword, user.password)
            if (!validPassword) return res.status(400).send('wrong password');

            user.email = req.body.email;
            user.username = req.body.username;
            user.age = req.body.age;
            user.password = req.body.newpassword;
            user.repeatpassword = req.body.newpassword;
            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(user.password,salt);
            await user.save();
            const token = jwt.sign({user : user}, 'jwtPrivateKey');
            res.json(token);
        } 
        else{
             console.log("nope")
            }
    },
    //end of edit user
        //delete user 
    deleteUser : async(req,res) => {
        let result = await User.deleteOne({_id: req.params.id})
        if(result.n > 0) {
            res.status(200).json('deleted successfully')
        } else {
            res.status(400).json('Couldn\'t delete User')
        }}
        //end of delete user 
    ,
        //get usersList
    getUsers: async (req,res) => {
        let usersList = await User.find();
        if(!usersList) {
            res.status(400).json('Couldn\'t get users list!')
        } else {
            res.json(usersList)
            }
        },
        //end of get usersList
    buyProduct : async (req,res)=>{
        console.log(req.body.id)
        let user = await User.findOne({_id : req.body._id});
        if(user) {
            user.balance = req.body.balance;
            await  user.save();
           return  res.status(200).send('updated balance');
        } 
        else {
            return res.status(400).send("not found user to update balance")
        }
    },
    //end of edit user

    addBalance : async (req,res)=>{
        console.log(req.body)
        let user = await User.findOne({_id : req.body._id});
        if(user) {
            user.balance = req.body.balance;
            await  user.save();
           return  res.status(200).json('updated balance');
        } 
        else {
           res.status(400).json("not found user to update balance")
        }
    },
    //end of addBalance

    boughtProduct :  async (req,res)=>{
        for(let i=0;  i< req.body.data.length; i++) {
            let product = {
                userId : req.body.id,
                name : req.body.data[i].name,
                size : req.body.data[i].size,
                color : req.body.data[i].color,
                weight : req.body.data[i].weight,
                description : req.body.data[i].description,
                price : req.body.data[i].price,
                shipping : req.body.data[i].shipping,
                brand : req.body.data[i].brand,
                src: req.body.data[i].src
            }

                const prod = await new boughtProd(product);
                await prod.save();
            }
        return res.json("created product");
    },
    //end boughtprod

    prevProducts :  async (req,res) => {
        let product = await boughtProd.find({userId : req.body.id});
        console.log(req.body.id)
        res.json(product)
    },

    contactMessage : async (req,res) =>{
        const {email,subject,text} = req.body;
    
        if(email.length > 0  && subject.length > 0 && text.length > 0  ){
            const message = await new userMessages(req.body);
            await message.save();
            return res.json('your message has been received')
        } else {
            return res.json('something went wrong, try again later')
        }
    }


}