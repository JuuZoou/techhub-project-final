const express = require('express');
const mongoose = require('mongoose');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Admin  = require('../models/admin');
const router = express.Router();
const fs = require('fs');

module.exports = { 
    registration :async (req,res)=>{
        let email = await Admin.findOne({ email : req.body.email });
        if(email) return  res.status(400).send('user already exists');
        const userDatas = _.pick(req.body,['email'],['password']);
        const user = await new Admin(userDatas);
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        await user.save();
        res.status(200).json(user);
    },
    //end of registration

    login : async (req,res)=>{
        let user = await Admin.findOne({email : req.body.email});
        if(!user) {
           return  res.status(553).send('invalid email or password');
        } else {
            const validPassword = await bcrypt.compare(req.body.password, user.password);
            if (!validPassword) {
                res.status(400).send('wrong password');
            }
            const token = jwt.sign({user:user},'jwtPrivateKey');
            return res.json(token);
        }
    },
    //end of login
}
