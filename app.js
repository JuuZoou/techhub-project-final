const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const lodash = require('lodash');
const concurrently = require('concurrently');
const jsonwebtoken = require('jsonwebtoken');
const nodemon = require('nodemon');
const cors = require('cors');
// const fs = require('fs');
const registration = require('./routes/user/registration');
const login = require('./routes/user/login');
const buyPR = require('./routes/user/buyProduct');
const boughtProd = require('./routes/user/boughtProd');
const createProduct = require('./routes/admin/products/createProduct');
const deleteProduct = require('./routes/admin/products/deleteProduct');
const editProduct = require('./routes/admin/products/editProduct');
const productList = require('./routes/admin/products/productList');
const ex = require('./routes/user/prevProducts')
const contactMessage = require('./routes/user/contactMessage')
const addBalance = require('./routes/user/addBalance');
const editUser = require('./routes/user/editUser');
const deleteUser = require('./routes/user/deleteUser');
const usersList = require('./routes/user/usersList');
const adminLogin = require('./routes/admin/adminLogin');
const adminRegistration= require('./routes/admin/adminRegistration');
// const show= require('./routes/admin/show');
const app = express();
app.use(express.urlencoded ({extended:true}));
app.use(express.json());
app.use(cors());

app.use('/registration', registration);
app.use('/login', login);
app.use('/buyProduct', buyPR);
app.use('/boughtProd', boughtProd);
app.use('/prevProducts', ex);
app.use('/createProduct', createProduct);
app.use('/productList', productList);
app.use('/deleteProduct', deleteProduct);
app.use('/editProduct', editProduct);
app.use('/addBalance', addBalance);
app.use('/editUser', editUser);
app.use('/deleteUser', deleteUser);
app.use('/usersList',usersList);
app.use('/adminLogin', adminLogin);
app.use('/adminRegistration', adminRegistration);
// app.use('/show', show);
app.use('/contactMessage', contactMessage);

mongoose.connect('mongodb://admin:admin1122@ds163694.mlab.com:63694/all',{useNewUrlParser: true})
.then(()=> console.log('connected'))
.catch((erroe)=>console.error(err));
mongoose.set('useCreateIndex', true);

const PORT = process.env.PORT || 3001;
app.get('/', (req,res)=>{    res.send('hello i am backend'+" "+`${PORT}`)
// fs.readFile('db.json', (err, data) => {  
//     if (err) throw err;
//     let student = JSON.parse(data);
//     console.log(student);
})
    


app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
  })