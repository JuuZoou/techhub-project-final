const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdminSchema = new Schema ({
    email : {
        type : String,
        minlength : 3 ,
        required : true
    },
    password : {
        type : String,
        minlength :3 ,
        required : true
    }
})

module.exports = Admin = mongoose.model("admin", AdminSchema)