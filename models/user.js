const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username :{
        type:String,
        minlength:4,
        required : true
    } ,
    email :{
        type:String,
        unique:true,
        minlength:6,
        required : true
    },
    password :{
        type:String,
        minlength:6,
        required : true
    },
    repeatpassword :{
        type:String,
        minlength:6,
        required : true
    },
    age :{
        type:Number,
        minlength:1,
        required : true
    },
    balance :{
        type:Number,
        minlength:1,
        required : true
    },

})

module.exports = User = mongoose.model("users", UserSchema);
