const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const boughtProdSchema = new Schema({
    userId : {
        type : ["integer","string"],
        minlength:3,
        required : true
    } ,
    name :{
        type:String,
        minlength:3,
        required : true
    },
    size :{
        type:String,
        minlength:1,
        required : true
    },
    color :{
        type:String,
        minlength:1,
        required : true
    },
    weight :{
        type:String,
        minlength:1,
        required : true
    },
    description :{
        type:String,
        minlength:3,
        required : true
    },
    price :{
        type:Number,
        minlength:1,
        required : true
    },
    shipping :{
        type:Number,
        minlength:1,
        required : true
    },
    brand :{
        type:String,
        minlength:3,
        required : true
    },
    src : {
        type:String,
        required: true
       
    }
})

module.exports = boughtProd = mongoose.model("boughtProd", boughtProdSchema);