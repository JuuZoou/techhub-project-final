const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema({
    email :{
        type:String,
        unique:false,
        minlength:3,
        required : true
    },
    subject :{
        type:String,
        unique:false,
        minlength:3,
        required : true
    },
    text :{
        type:String,
        unique:false,
        minlength:3,
        required : true
    },
})

module.exports = userMessages = mongoose.model("userMessages", messageSchema);