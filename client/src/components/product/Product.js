import React from 'react';
import Detailed from '../view/detailed/Detailed';

class Popup extends React.Component {
  imgFunc = (data) => {
    let expandedIMG = document.getElementById('expandedIMG');
    expandedIMG.src = data;
  };
  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <button onClick={this.props.closePopup}>close me</button>
          <section className='product product--detailed'>
                <div className='wrapper fl fl_jus_bet fl_ali_sta'>
                    <div className='product--left'>
                        <div className='product__image-view'>
                            <img className='product__image-expand' id='expandedIMG' src={this.props.product.src[0]} alt='Product'/>
                        </div>
                        <div className='product__image-view--small'>
                            <img onClick={()=>this.imgFunc(this.props.product.src[0])} src={this.props.product.src[0]} alt='Product'/>
                            <img onClick={()=>this.imgFunc(this.props.product.src[1])} src={this.props.product.src[1]} alt='Product'/>
                            <img onClick={()=>this.imgFunc(this.props.product.src[2])} src={this.props.product.src[2]} alt='Product'/>
                        </div>
                    </div>
                    <div className='product--right'>
                        <div className='product__column'>
                            <h3 className='product__title'>Cropped Leather Jacket</h3>
                            <p className='product__details'>
                                An off-duty alternative to our Classic collection. Inspired by the original "ballerina," the shoe is handcrafted of a soft nappa leather, finished with grosgrain trim and a working bow. Available in European sizes and three width options for the perfect fit, even on your most casual days.
                            </p>
                        </div>
                        <div className='product__column'>
                            <div className='product__row'>
                                <label className='product__list'>Colour
                                    <select className='product__list-select'>
                                        <option value="volvo">White</option>
                                        <option value="saab">Black</option>
                                    </select>
                                </label>
                                <label className='product__list'>Size
                                    <select className='product__list-select'>
                                        <option value="volvo">Small</option>
                                        <option value="saab">Medium</option>
                                    </select>
                                </label>
                                <label className='product__list'>Weight
                                    <span className='product__list-select'>{this.props.product.weight} kg</span>
                                </label>
                            </div>
                            <div className='product__row'>
                                <span className='product__add-cart' onClick={()=> this.props.chooseProduct(this.props.product)}>ADD TO BAG</span>
                                <span className='product__add-favorite'>
                                    <i className='icon icon--favorite'></i>
                                </span>
                                <span className='product__price'>{this.props.product.price}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
      </div>
    );
  }
}

class Product extends React.Component {
  constructor() {
    super();
    this.state = {
      showPopup: false
    };
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }
  handleChooseProduct = (product) =>{
    //if user exists, add choosed products to cart, that we will see in profile page
    if(!localStorage.getItem('user')) {
        return alert("login first")
    }
        let products = { id:product.id, name :product.name, size:product.size, color:product.color, 
          weight:product.weight, description:product.description,price : product.price, shipping : product.shipping, brand:product.brand, src:product.src};
      if(localStorage.getItem("choosedProducts")){
        const storage = JSON.parse(localStorage.getItem("choosedProducts"));
        storage.push(products)
        localStorage.setItem("choosedProducts", JSON.stringify(storage));
      } else {
        let choosedProducts = [];
        choosedProducts.push(products);
        localStorage.setItem("choosedProducts", JSON.stringify(choosedProducts));
      }
  }
  componentDidMount=()=>{
    this.props.getProdFunc();
  }
  render(){
    const { getProdRes } = this.props;
    return(
      <div className='catalogue__inner' id='catalogueView'>
          {getProdRes.map((product,i)=> { 
            return <article className='product' key={i}>
              <div className='product__image' style={{backgroundImage: `url(${product.src[0]})`}}> 
                <div className='product__more'>
                  {/* <Link to='/detailed' className='product__more-text'>Details</Link> */}
                  <div className='product__more-text' onClick={this.togglePopup.bind(this)}>Details</div>
                  {this.state.showPopup ? 
                    <Popup
                      text='Close Me'
                      closePopup={this.togglePopup.bind(this)}
                      product={product}
                      chooseProduct={this.handleChooseProduct}
                    />
                    : null
                  }
                  <span onClick={()=>this.handleChooseProduct(product)} className='product__more-text'>Add to Cart
                  </span>
                </div>
              </div>
              <div className='product__column'>
                <div className='product__row'>
                  <h3 className='product__title'>{product.name}</h3>
                  <span className='product__favorite'>
                    <i className='icon icon--favorite'></i>
                  </span>
                </div>
                <span className='product__price'>{product.price}</span>
              </div>
            </article>
          })
        } 
      </div>
    )
  }
}

export default Product;