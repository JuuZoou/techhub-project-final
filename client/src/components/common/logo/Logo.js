import React from 'react';
import { Link } from "@reach/router"

class Logo extends React.Component {
  render(){
    return (
      <h1 className='logo'><Link to="/">CoolAttitude</Link></h1>
    )
  }
}

export default Logo;