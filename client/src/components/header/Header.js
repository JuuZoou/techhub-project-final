import React from 'react';
import Logo from '../common/logo/Logo';
import Navigation from './navigation/Navigation';
import UserPanel from './userpanel/UserPanel';

class Header extends React.Component {
  render(){
    return (
      <div className='header'>
        <div className='header__top'>
          <div className='wrapper fl fl_jus_bet fl_ali_cen'>
            <Logo/>
            <UserPanel/>
          </div>
        </div>
        <Navigation/>
      </div>
    )
  }
}

export default Header;