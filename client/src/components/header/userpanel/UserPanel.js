import React from 'react';
import { Link } from '@reach/router';

class UserPanel extends React.Component {
  hanldeLogOut = ()=>{
    if(localStorage.getItem("user")) {
        localStorage.clear();
        setTimeout(function(){
            window.location = "http://localhost:3000/";
        },100)
    }
  }
  render() {
    if(!localStorage.getItem('user')){
      return (
        <div className='user-panel' id='userPanel'>
            <Link to='/auth/login' className="user-panel__item">Login</Link>
            <Link to='/auth/signup' className="user-panel__item">Sign Up</Link>
            <Link to='/contact' className="user-panel__item">Contact</Link>
            <Link to='/user/cart' className="user-panel__item user-panel__item--cart">
                <i className="icon icon--shopping-cart"></i>
            </Link>
        </div>
      )
    }else if(localStorage.getItem('user')){
      return (
        <div className='user-panel' id='userPanel'>
            <Link to='/user' className="user-panel__item">Profile</Link>
            <span to='/user' className="user-panel__item" onClick={this.hanldeLogOut}>Log Out</span>
            <Link to='/contact' className="user-panel__item">Contact</Link>
            <Link to='/user/cart' className="user-panel__item user-panel__item--cart">
                <i className="icon icon--shopping-cart"></i>
            </Link>
        </div>
      )
    }
  }
}

export default UserPanel;