import React from 'react';
import axios from 'axios';

class SignUp extends React.Component {
  state = {
      registrationErr : [],
      registrationSuccess:[],
  }
  handleUsernameCheck = (username) => {
    if( username !=="" && username !== null && username.length > 3) {
      return true;
    } else {
      return alert("wrong username");
    }
  }
  handleEmailValidation = (email)=>{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = re.test(email);
    const index = email.indexOf("@");
    if (isValid && index>-1) {
    return true;
    } else if(!isValid) {
    return alert("email is not valid");
    }
  }
  handleRegistration = (event)=>{
    event.preventDefault();
    const {username,email,password,repeatpassword,age,balance} = event.target;
    const userData = {
      username : username.value,
      email : email.value,
      password : password.value,
      repeatpassword:repeatpassword.value,
      age : age.value,
      balance : balance.value,
    }
    //checking username and email with functions
    this.handleUsernameCheck(userData.username) 
    this.handleEmailValidation(userData.email)
    //fetching
    axios.post("http://localhost:3001/registration", userData)
    .then(res => {
      this.setState({registrationSuccess:"you have successfully registered"})
    })
    .catch(err =>{
      this.setState({registrationErr : "this user already exists"})
    })
  }
  render(){
    const {registrationErr,registrationSuccess } = this.state;
    return (
      <div className='login'>
        <div className='login__inner'>
          <div className='login__heading'>
            <h2>Sign Up</h2>
            {registrationErr ? <p className='alert'>{registrationErr}</p> : <p></p>}
            {registrationSuccess ? <p className='alert'>{registrationSuccess}</p> : <p></p>}
          </div>
          <form className='form' onSubmit={this.handleRegistration}>
              <lable className='lable'>Email
                  <input className='form__input' type='text' name="email" required/>
              </lable>
              <lable className='lable'>User Name
                  <input className='form__input' type='text' name="username"  required/>
              </lable>
              <lable className='lable'>Age
                  <input className='form__input' type='number' name="age" required/>
              </lable>
              <lable className='lable'>Balance
                  <input className='form__input' type='number' name="balance" required/>
              </lable>
              <lable className='lable'>Password
                  <input className='form__input' type='password' name="password"  required/>
              </lable>
              <lable className='lable'>Password Again
                  <input className='form__input' type='password' name="repeatpassword" required/>
              </lable>
              <div className='form__row'>
              <div className='form__row--left'>
                <label className='lable lable--checkbox' htmlFor='agree'>
                  <input className='form__checkbox' id='agree' type='checkbox'/>I Agree Terms and Conditions
                </label>
              </div>
              </div>
              <button className='form__button' type="submit">Sign Up</button>
          </form>
        </div>
      </div>
    )
  }
}

export default SignUp;