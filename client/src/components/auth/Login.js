import React from 'react';
import axios from 'axios';

class Login extends React.Component {
  state ={
    loginError : [],
  }
  handleLogIn = (event)=> {
    event.preventDefault();
    if(localStorage.getItem('user')) {
      return this.setState({loginError:"you are already loggedin"})
    }
    const {email,password} = event.target;
    const userData = {
      email :email.value,
      password : password.value,
    }
    axios.post("http://localhost:3001/login", userData)
    .then(token => {
      localStorage.setItem("user", token.data)
      // this.handleUserGet()
      window.location = "http://localhost:3000/";
    })
    .catch(error=>this.setState({loginError : "invalid email or password"}))
  }
  render (){
    return (
      <div className='login'>
        <div className='login__inner'>
          <div className='login__heading'>
            <h2>Login</h2>
            {this.state.loginError ? <p className='alert'>{this.state.loginError}</p> : <p className='alert'></p>}
          </div>
          <form className='form' onSubmit={ this.handleLogIn }>
              <lable className='lable'>Email
                  <input className='form__input' type='text' name="email" required/>
              </lable>
              <lable className='lable'>Password
                  <input className='form__input' type='password' name="password" required/>
              </lable>
              <div className='form__row'>
                <div className='form__row--left'>
                  <label className='lable lable--checkbox' for='remember'>
                    <input className='form__checkbox' id='remember' type='checkbox'/>Remember me
                  </label>
                </div>
                <div className='form__row--right'></div>
              </div>
              <button className='form__button' type="submit">Log In</button>
          </form>
        </div>
      </div>
    )
  }
}

export default Login;