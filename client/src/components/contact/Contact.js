import React from 'react';
import axios from 'axios';

class Contact extends React.Component {
    state = {
        succMessage :'',
        errMessage :''
    }
    handleContactMessage  = (event)=> {
        //sending contact message 
        event.preventDefault();
        const {email,subject,text } = event.target;
        const contMessage = {
          email:email.value,
          subject:subject.value,
          text : text.value
        }
        axios.post("http://localhost:3001/contactMessage",contMessage)
        .then(res=>this.setState({succMessage : res.data}))
        .catch(error =>this.setState({errMessage : error.response}))
    }
    render(){
        const { errMessage, succMessage } = this.state;
        return (
            <div className='contact' id='Content'>
                <div className='contact__inner'>
                    <h2 className='contact__heading'>Contact Us</h2>
                    {succMessage ? <span className='alert'>{succMessage}</span> : <span></span>}
                    {errMessage ? <span className='alert'>{errMessage}</span> : <span></span>}
                    <div className='contact__container'>
                        <div className='contact--left'>
                            <span className='contact__title'>We are always ready to cooperate. Write to us and we will contact you.</span>
                            <ul className='contact__list'>
                                <li className='contact__list-title'>EMAIL</li>
                                <li className='contact__list-item'>contact@coolattitude.ga</li>
                            </ul>
                            <ul className='contact__list'>
                                <li className='contact__list-title'>PHONE</li>
                                <li className='contact__list-item'>(+995)598-91-31-11</li>
                            </ul>
                            <ul className='contact__list'>
                                <li className='contact__list-title'>ADDRESS</li>
                                <li className='contact__list-item'>25 Boehm Cape Suite 549</li>
                                <li className='contact__list-item'>Alexiston, United States</li>
                            </ul>
                        </div>
                        <div className='contact--right'>
                            <form className='form form--contact' onSubmit={this.handleContactMessage}>
                                <lable className='lable'>Email Address
                                    <input className='form__input' type='email' name="email" required/>
                                </lable>
                                <lable className='lable'>Subject
                                    <input className='form__input' type='text' name="subject" required/>
                                </lable>
                                <lable className='lable'>Message
                                    <textarea className='form__textarea' name='text' required></textarea>
                                </lable>
                                <button className='form__button' type="submit">SEND</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact;