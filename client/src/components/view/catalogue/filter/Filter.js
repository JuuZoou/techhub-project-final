import React from 'react';

class Filter extends React.Component {
    render(){
        const { clearFilter, priceFilter, handleSizeFilter,brandFilter,searchProduct } = this.props;

        const filterFunc = (arg) => {
            let catalogueView = document.getElementById('catalogueView');
            if( arg === 'four' ){
                catalogueView.style.gridTemplateColumns = 'repeat(4, 1fr)';
            } else if( arg === 'two' ){
                catalogueView.style.gridTemplateColumns = 'repeat(2, 1fr)';
            }
        }
        return (
            <div className='catalogue__filter'>
                <div className='catalogue__filter--left'>
                    <select name='brand' className='catalogue__filter-list' onChange={brandFilter}>
                        <option>Brand</option>
                        <option value="dior">Dior</option>
                        <option value="chanel">Chanel</option>
                    </select>
                    <select name='size' className='catalogue__filter-list' onChange={handleSizeFilter}>
                        <option value="volvo">Size</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                        <option value="32">32</option>
                        <option value="33">33</option>
                        <option value="34">34</option>
                        <option value="35">35</option>
                        <option value="36">36</option>
                        <option value="38">38</option>
                        <option value="39">39</option>
                        <option value="40">40</option>
                        <option value="41">41</option>
                        <option value="42">42</option>
                        <option value="43">43</option>
                        <option value="44">44</option>
                    </select>
                    <select name='price' className='catalogue__filter-list' onChange={priceFilter}>
                        <option>Price</option>
                        <option value="ascending">Low to Hight</option>
                        <option value="descending">Hight to Low</option>
                    </select>
                    <button className='catalogue__filter-clear' onClick={clearFilter}>Clear Filter</button>
                </div>
                <div className='catalogue__filter--right'>
                    <input className='catalogue__filter-search' type='text' placeholder='Enter search keyword' onChange={searchProduct}/>
                    <span className='catalogue__filter-view' onClick={()=> {filterFunc('four')}}>
                        <i className='icon icon--view-small'></i>
                    </span>
                    <span className='catalogue__filter-view' onClick={()=> {filterFunc('two')}}>
                        <i className='icon icon--view-big'></i>
                    </span>
                </div>
            </div>
        )
    }
}

export default Filter;