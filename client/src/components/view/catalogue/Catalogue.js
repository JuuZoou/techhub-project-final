import React from 'react';
import Product from '../../product/Product';
import Filter from './filter/Filter';
import axios from 'axios';

class Catalogue extends React.Component {
    state = {
        Products : []
    }

    // Filters
    handlePriceFilter = (event) => {
    event.preventDefault();
    let value = event.target.value;
    let myProducts = [...this.state.Products];
    if(value==="ascending") {
        let ascOrd = myProducts.sort(function(a,b){
        return a.price - b.price
    })
    this.setState({Products : ascOrd},function(){
        console.log(this.state.Products)
    })      
    localStorage.setItem("Products", JSON.stringify(ascOrd)) 
    }  else if (value==="descending"){
        let descOrd = myProducts.sort(function(a,b){
            return b.price - a.price
        })
        this.setState({Products : descOrd},function(){
        console.log(this.state.Products)
        })      
        localStorage.setItem("Products", JSON.stringify(descOrd)) 
        }
    }
    handleSizeFilter = (event) => {
    event.preventDefault();
    let size = Number(event.target.value);
    console.log(size)
        let myProducts = [...this.state.Products];
        let array = myProducts.filter(shoe => shoe.size.toString().indexOf(size)>-1);
        array.map( shoe => {
        let item = {
            id:shoe.id,
            name: shoe.name, 
            size:shoe.size, 
            color:shoe.color,
            weight:shoe.weight, 
            description:shoe.description,
            price:shoe.price ,
            shipping:shoe.shipping,
            brand:shoe.brand}
            return myProducts.push(item)})
            this.setState({Products : array},function(){
            console.log(this.state.Products)
            })      
            localStorage.setItem("Products", JSON.stringify(array))
    }
    handleBrandFilter = (event) => {
    event.preventDefault();
    let brand = event.target.value;
        let myProducts = [...this.state.Products];
        let array = myProducts.filter(shoe =>shoe.brand === brand);
        array.map( shoe => {
        let item = {
            id:shoe.id,
            name: shoe.name, 
            size:shoe.size, 
            color:shoe.color,
            weight:shoe.weight, 
            description:shoe.description,
            price:shoe.price ,
            shipping:shoe.shipping,
            brand:shoe.brand}
            return myProducts.push(item)})
            this.setState({Products : array},function(){
            console.log(this.state.Products)
            })      
            localStorage.setItem("Products", JSON.stringify(array))
    }

    // Products
    handleChooseProduct = (product) =>{
        //if user exists, add choosed products to cart, that we will see in profile page
        if(!localStorage.getItem('user')) {
          return alert("login first")
        }
        if(localStorage.getItem("choosedProducts")){
            const storage = JSON.parse(localStorage.getItem("choosedProducts"))
            let products = {id:product.id, name :product.name, size:product.size, color:product.color,
            weight:product.weight, description:product.description,price:product.price, 
            shipping : product.shipping, brand:product.brand,src:product.src };
            storage.push(products)
            localStorage.setItem("choosedProducts", JSON.stringify(storage))
        } else {
          let choosedProducts = [];
           let products = {id:product.id, name :product.name, size:product.size, color:product.color,
           weight:product.weight, description:product.description,price:product.price, 
           shipping : product.shipping, brand:product.brand,src:product.src };
          choosedProducts.push(products);
          localStorage.setItem("choosedProducts", JSON.stringify(choosedProducts))
        }
    }
    handleShowProducts = () => {
        //bringing products from API db
        axios.get('http://localhost:4000/db')
        .then(res =>{
            let prods =[];
            for(let key in res.data){
            res.data[key].map( shoe => {
                let item = {id:shoe.id,name: shoe.name, size:shoe.size, 
                color:shoe.color,weight:shoe.weight,description:shoe.description,
                price:shoe.price,shipping:shoe.shipping,category:shoe.category,brand:shoe.brand,
                src:shoe.src}
                return prods.push(item)
                })}
            this.setState({Products:prods})
            localStorage.setItem("Products", JSON.stringify(prods))
        })
        .catch(error => console.error(error))
    }
    handleSearchProducts= (event)=>{
        event.preventDefault();
        let search = event.target.value;
        console.log(search)
        axios.get('http://localhost:4000/db')
        .then(res =>{
            let prods =[];
              for(let key in res.data){
                res.data[key].map( shoe => {
                  let item = {id:shoe.id,name: shoe.name,size:shoe.size, 
                    color:shoe.color,weight:shoe.weight, description:shoe.description,
                    price:shoe.price,shipping:shoe.shipping,brand:shoe.brand,src:shoe.src}
                  return prods.push(item)
              })}
          let includes = prods.filter(prod=>prod.name.includes(search))
          this.setState({Products:includes},function(){console.log(this.state.Products)})
        })
        .catch(err=>console.log(err.response))
    }

    componentDidMount = ()=>{
        this.handleShowProducts();
    }

    render(){
        const { handlePriceFilter,handleSizeFilter,handleBrandFilter,
                handleShowProducts,handleChooseProduct,handleSearchProducts } = this;
        return (
            <section className='catalogue'>
                <div className='wrapper fl fl_dir_col fl_ali_cen'>
                    <h2 className='catalogue__heading'>Catalogue</h2>
                    <Filter
                        prods={this.state.Products}
                        priceFilter={handlePriceFilter}
                        handleSizeFilter={handleSizeFilter}
                        brandFilter={handleBrandFilter}
                        clearFilter={handleShowProducts}
                        searchProduct={handleSearchProducts}
                    />
                    <div className='catalogue__outer'>
                        <Product 
                          getProdFunc={handleShowProducts}
                          getProdRes={this.state.Products}
                          getChooseProd={handleChooseProduct}
                        />
                    </div>
                    <div className='catalogue__load'>
                        <span className='catalogue__load-more'>Load More</span>
                    </div>
                </div>
            </section>
        )
    }
}

export default Catalogue;