import React, { Component } from 'react';

class Accordion extends Component {
    
    accFunc = (id) => {
        let button = document.getElementsByClassName('accordion__button');
        let content = button[id].nextElementSibling;
        if (content.style.display === "block") {
            button[id].style.borderBottom = "1px solid #F3F1EF";
            content.style.borderBottom = "1px solid transparent";
            content.style.display = "none";
            button[id].style.background = "transparent url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAABHNCSVQICAgIfAhkiAAAACRJREFUKJFjYMABNm3a9B+XHBMuCXxgOGpixBdKJIPRICcCAAAKoAlVrKK16gAAAABJRU5ErkJggg==') no-repeat top 50% right 10px";
        } else {
            button[id].style.borderBottom = "1px solid transparent";
            content.style.borderBottom = "1px solid #F3F1EF";
            content.style.display = "block";
            button[id].style.background = "transparent url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAABCAYAAAAFKSQHAAAABHNCSVQICAgIfAhkiAAAABBJREFUCJlj3LRp038GEgEAm5sDF8+VjsQAAAAASUVORK5CYII=') no-repeat top 50% right 10px";
        }
    }

    render() {
        return (
            <div className='accordion'>
                <button className='accordion__button' onClick={() => {this.accFunc(0)}}>Product information</button>
                <div className='accordion__content'>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div>
                <button className='accordion__button' onClick={() => {this.accFunc(1)}}>Deliveries and returns</button>
                <div className='accordion__content'>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div>
            </div>
        )
    }
}

export default Accordion;