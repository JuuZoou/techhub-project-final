import React from 'react';
import { Link } from '@reach/router';

import ImageSlider from './ImageSlider';
import Accordion from './Accordion';

class Detailed extends React.Component {
    render() {
        return (
            <section className='product product--detailed'>
                <div className='wrapper fl fl_jus_bet fl_ali_sta'>
                    <ImageSlider/>
                    <div className='product--right'>
                        <div className='product__link'>
                            <Link className='product__link-item' to='/'>Shop</Link>
                            <Link className='product__link-item' to='/'>For Woman</Link>
                            <Link className='product__link-item' to='/'>T-Shirt</Link>
                        </div>
                        <div className='product__column'>
                            <h3 className='product__title'>Cropped Leather Jacket</h3>
                            <p className='product__details'>
                                An off-duty alternative to our Classic collection. Inspired by the original "ballerina," the shoe is handcrafted of a soft nappa leather, finished with grosgrain trim and a working bow. Available in European sizes and three width options for the perfect fit, even on your most casual days.
                            </p>
                        </div>
                        <div className='product__column'>
                            <div className='product__row'>
                                <label className='product__list'>Colour
                                    <select className='product__list-select'>
                                        <option value="volvo">White</option>
                                        <option value="saab">Black</option>
                                    </select>
                                </label>
                                <label className='product__list'>Size
                                    <select className='product__list-select'>
                                        <option value="volvo">Small</option>
                                        <option value="saab">Medium</option>
                                    </select>
                                </label>
                                <label className='product__list'>Weight
                                    <span className='product__list-select'>2.56 KG</span>
                                </label>
                            </div>
                            <div className='product__row'>
                                <span className='product__add-cart'>ADD TO BAG</span>
                                <span className='product__add-favorite'>
                                    <i className='icon icon--favorite'></i>
                                </span>
                                <span className='product__price'>$25.00</span>
                            </div>
                        </div>
                        <Accordion/>
                    </div>
                </div>
            </section>
        )
    }
}

export default Detailed;