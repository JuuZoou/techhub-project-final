import React from 'react';

class ImageSlider extends React.Component {
    imgFunc = (data) => {
        let expandedIMG = document.getElementById('expandedIMG');
        expandedIMG.src = data;
    };
    articleIMG = {
        image1: 'https://dm.victoriassecret.com/p/380x507/tif/15/c0/15c0dbbbf3814c0aad5d7e130d7850c7/391077HAX_OM_F.jpg',
        image2: 'https://dm.victoriassecret.com/p/380x507/tif/64/2c/642c7db3695b473f82b0cc88df973272/393172HAS_OM_F.jpg',
        image3: 'https://dm.victoriassecret.com/p/380x507/tif/cf/2a/cf2a1ba18b6e429ea15ccfea3f5fd71b/39317272Y_OM_F.jpg'
    };
    render() {
        return (
            <div className='product--left'>
                <div className='product__image-view'>
                    <img className='product__image-expand' id='expandedIMG' src={this.articleIMG.image1} alt='Product'/>
                </div>
                <div className='product__image-view--small'>
                    <img onClick={()=>this.imgFunc(this.articleIMG.image2)} src={this.articleIMG.image2} alt='Product'/>
                    <img onClick={()=>this.imgFunc(this.articleIMG.image3)} src={this.articleIMG.image3} alt='Product'/>
                    <img onClick={()=>this.imgFunc(this.articleIMG.image1)} src={this.articleIMG.image1} alt='Product'/>
                </div>
            </div>
        )
    }
}

export default ImageSlider;