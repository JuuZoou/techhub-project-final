import React from 'react';
import axios from 'axios';

class Admin extends React.Component {
    state= {
        admloginError: "",
    }
    handleAdminLogin = (event)=>{       
            event.preventDefault();
            const {email,password} = event.target;
            const userData = {
              email :email.value,
              password : password.value,
            }
            axios.post("http://localhost:3001/AdminLogin", userData)
            .then( token => {
              localStorage.setItem("admin", token.data)
              window.location = "http://localhost:3000/adminProfile";
            })
            // admloginError  
            .catch(error => this.setState({admloginError : "wrong datas"}))
        
        //admin login 
       
    }
      
    render(){
        return(
            // <div>
            //     <p>log in </p>
            //     {this.state.admloginError ? <p>{this.state.admloginError}</p> : <p></p>}
            //     <form onSubmit={this.handleAdminLogin}>
            //         <label>email</label>
            //         <input  name="email"/>   <br />
            //         <label>password</label>
            //         <input name="password"/>   <br />
            //         <button>submit</button>
            //     </form>
            // </div>
            <div className='login'>
                <div className='login__inner'>
                <div className='login__heading'>
                    <h2>Admin Login</h2>
                    {this.state.admloginError ? <p>{this.state.admloginError}</p> : <p></p>}
                </div>
                <form className='form' onSubmit={this.handleAdminLogin}>
                    <lable className='lable'>Email
                        <input className='form__input' type='text' name="email" required/>
                    </lable>
                    <lable className='lable'>Password
                        <input className='form__input' type='password' name="password" required/>
                    </lable>
                    <button className='form__button' type="submit">Log In</button>
                </form>
                </div>
            </div>
        )
    }
}

export default Admin;