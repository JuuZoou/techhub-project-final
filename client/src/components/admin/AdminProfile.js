import React from 'react';
import axios from 'axios';
const uniqid = require('uniqid');
const jwt_decode = require('jwt-decode');

class AdminProfile extends React.Component{
    state = {
        Products: [],
        adminDatas : [{
            email:'',
            password:''
          }],
        allUsers : []

    }
    handleAdminGet = ()=> {
        //get admin if it exists in localstorage
        if(localStorage.getItem('admin')){
          const adminDatas = jwt_decode(localStorage.getItem('admin'));
          const stateCopy = [...this.state.adminDatas];
          stateCopy.email = adminDatas.user.email;
          stateCopy.password = adminDatas.user.password;
          this.setState({adminDatas:stateCopy})
         }
      }
    hanldeAdminLogOut = ()=>{
        //admin logout
        if(localStorage.getItem("admin")) {
            localStorage.clear();
            setTimeout(function(){
                window.location = "http://localhost:3000/";
              },100)
        }
    }
    handleShowProducts = () => {
        //bringing products from API db
        axios.get('http://localhost:4000/db')
        .then(res =>{
            let prods =[];
            for(let key in res.data){
            res.data[key].map( shoe => {
                let item = {id:shoe.id,name: shoe.name, size:shoe.size, 
                color:shoe.color,weight:shoe.weight,description:shoe.description,
                price:shoe.price,shipping:shoe.shipping,category:shoe.category,brand:shoe.brand,
                src:shoe.src}
                return prods.push(item)
                })}
            this.setState({Products:prods})
            localStorage.setItem("Products", JSON.stringify(prods))
        })
        .catch(error => console.error(error))
    }
    handleAdminCreateProd = (event) => {
    event.preventDefault();
    const {name,size,color,weight,description,price,shipping,brand,category,src1,src2,src3}=event.target;
    const product = {
      id:uniqid(),
      name:name.value,
      size:size.value,
      color:color.value,
      weight:weight.value,
      description:description.value,
      price:price.value,
      shipping:shipping.value,
      brand:brand.value,
      category:category.value,
      src:[src1.value, src2.value,src3.value]
    }
    console.log(product)
    console.log(category.value)
    axios.post(`http://localhost:3001/createProduct/${category.value}`,product )
    .then(res=>res.data)
    .then(res=>console.log(res))
    .catch(error=>console.log(error.response))

  }
  handleEditUser = (event,id)=>{
    //edit user by user
    event.preventDefault();
    const {username,email,age,newpassword,oldpassword } = event.target;
    const newDatas = {email:email.value,username:username.value,age:age.value,
        newpassword : newpassword.value,oldpassword:oldpassword.value,
        id:id};

    axios.post("http://localhost:3001/editUser" , newDatas)
    .then(token => { })
    .catch(error =>console.log(error.response))
}
  handleAdminDelProd = (id,category)=>{
    console.log(id,category)
    axios.delete(`http://localhost:3001/deleteProduct/${category}/${id}`)
    .then(res=>console.log(res.data))
    .catch(error=>console.log(error.response))
  }
  handleAdminDelUser = (user) => {
    axios.delete(`http://localhost:3001/deleteUser/${user._id}`,)
    .then(res=>console.log(res.data))
    .catch(err=>console.log(err.response))
  }
  handleAdminGetUsers = () => {
      axios.get(`http://localhost:3001/usersList`)
      .then(res=>this.setState({allUsers : res.data}),function(){
          console.log(this.state.allUsers)
      })
      .catch(err => console.log(err.response))
  }
  handleAdminEditProd = (id,category)=>{
    console.log(id,category)
    axios.put(`http://localhost:3001/editProduct/${category}/${id}`)
    .then(res=>console.log(res.data))
    .catch(error=>console.log(error.response))
  }
  componentDidMount = ()=>{
    this.handleShowProducts();
    this.handleAdminGet();
    this.handleAdminGetUsers();
}
    render(){
        const {handleShowProducts,hanldeAdminLogOut,
            handleAdminCreateProd,handleAdminEditProd,
            handleAdminDelProd }= this;
        return(
        <div className='wrapper'>
            <div className='admin-profile'>
                <p> Admin Email: <span>{this.state.adminDatas.email}</span></p><br />
                <button onClick={hanldeAdminLogOut}>Log out </button><br />
                <br /><br /><br />
            </div>
            <div className='contact__container'>
                <div className='contact--left'>
                    <div class="login__heading"><h2>Add Product</h2></div>
                    <form onSubmit={handleAdminCreateProd}>
                        <label className='lable'>category
                            <select name="category">
                                <option>choose one</option>
                                <option name="shoes" value="shoes">shoes</option>
                                <option name="bags" value="bags">bags</option>
                                <option name="accessories" value="accessories">accessories</option>
                            </select>
                        </label>
                        <label className='lable'>name
                            <input className='form__input' name="name"required/><br />
                        </label>
                        <label className='lable'>size</label>
                            <input className='form__input' name="size" type="number" required/><br />
                        <label className='lable'>color</label>
                            <input className='form__input' name="color" required/><br />
                        <label className='lable'>weight</label>
                            <input className='form__input' name="weight" type="number" required/><br />
                        <label className='lable'>description</label>
                            <input className='form__input' name="description" required/><br />
                        <label className='lable'>price</label>
                            <input className='form__input' name="price" type="number" required/><br />
                        <label className='lable'>shipping</label>
                            <input  name="shipping" type="number" required/><br />
                        <label className='lable'>brand</label>
                            <input className='form__input' name="brand" required/><br />
                        <label className='lable'>img links</label>
                            <input className='form__input' name="src1" type="text" required/><br />
                        <label className='lable'>img links</label>
                            <input className='form__input' name="src2" type="text" required/><br />
                        <label className='lable'>img links</label>
                            <input className='form__input' name="src3" type="text" required/><br />
                        <button  className='form__button' type="submit">submit</button>
                    </form>
                </div>
                <div className='contact--right'>
                    <div class="login__heading"><h2>Edit Product</h2></div>
                    <form onSubmit={handleAdminEditProd}>
                        <label className='lable'>category</label>
                        <select name="category">
                            <option>choose one</option>
                            <option name="shoes" value="shoes">shoes</option>
                            <option name="bags" value="bags">bags</option>
                            <option name="accessories" value="accessories">accessories</option>
                        </select>
                        <label className='lable'>name</label>
                            <input className='form__input' name="name"/><br />
                        <label className='lable'>size</label>
                            <input className='form__input' name="size" type="number" /><br />
                        <label className='lable'>color</label>
                            <input className='form__input' name="color" /><br />
                        <label className='lable'>weight</label>
                            <input className='form__input' name="weight" type="number" /><br />
                        <label className='lable'>description</label>
                            <input className='form__input' name="description" /><br />
                        <label className='lable'>price</label>
                            <input className='form__input' name="price" type="number" /><br />
                        <label className='lable'>shipping</label>
                            <input className='form__input' name="shipping" type="number" /><br />
                        <label className='lable'>brand</label>
                            <input className='form__input' name="brand" /><br />
                        <button className='form__button' type="submit">submit</button>
                    </form>
                </div>
            </div>
            
            <br /><br /><br />

        {this.state.Products ? this.state.Products.map((product,i)=> {
            return <div className='admin-product' key={i}>
                     <b>id: {product.id}</b> <br />
                     <b>category: {product.category}</b> <br />
                     <b>name: {product.name}</b> <br /> 
                     <b> size: {product.size} </b> <br /> 
                     <b> color: {product.color} </b> <br /> 
                     <b> weight: {product.weight} </b> <br /> 
                     <b> description: {product.description} </b> <br /> 
                     <b> price: {product.price} </b> <br /> 
                     <b> brand: {product.brand} </b> <br /> 
                     <button onClick={()=>handleAdminDelProd(product.id,product.category )} > delete product </button> <br />
                 </div>
               }) : <p></p>
           }   
   
            {this.state.allUsers ?  this.state.allUsers.map((user,i)=>{
                return <div className='admin-product' key={i}>
                        <h3>user datas :</h3>
                        <p>user id : {user._id}</p>
                        <p> user name :{user.username}</p>
                        <p> user email :{user.email}</p>
                        <button onClick={()=>this.handleAdminDelUser(user)}>delete user</button><br />

                        <p>edit user form </p>
                        <form onSubmit={(event)=>this.handleEditUser(event,user._id)}>
                            <label>email</label>
                            <input type="text" name="email"/>
                            <label>username</label>
                            <input type="text" name="username"/>
                            <label>age</label>
                            <input type="number" name="age"/>
                            <label>old password</label>
                            <input name="oldpassword" type="text"/>
                            <label>new password</label>
                            <input name="newpassword" type="text"/>
                            <button type="submit">submit</button>
                        </form>
                        <br/><br/><br/><br/>
                        </div>
            }) : <p> no users </p>
        }        
        </div>
        
        )
    }

    
}

export default AdminProfile;