import React from 'react';
import axios from "axios";
import jwt from 'jsonwebtoken';
import jwt_decode from 'jwt-decode';

class Cart extends React.Component {
    state = {
        userDatas : [{
            _id:"",
            username :'',
            email:'',
            password:'',
            age:'',
            birthday:'',
            balance:''
          }],
        choosedProducts : [],
        addedProducts :[],
        prevProducts :[],
        
    }
    handleUserGet = () => {
        //get users if it exists in locastorage
        if(localStorage.getItem('user')){
            const userDatas = jwt_decode(localStorage.getItem('user'));
            const stateCopy = [...this.state.userDatas];
            stateCopy.username = userDatas.user.username;
            stateCopy._id = userDatas.user._id;
            stateCopy.email = userDatas.user.email;
            stateCopy.password = userDatas.user.password;
            stateCopy.age = userDatas.user.age;
            stateCopy.balance = userDatas.user.balance;
            stateCopy.birthday = userDatas.user.birthday;
            this.setState({userDatas:stateCopy},function(){
                console.log(userDatas )
            })
        } 
    }
    handleChooseProduct = () =>{
        //if user exists, add choosed products to cart, that we will see in profile page
        if(!localStorage.getItem('user')) { return alert("login first") }
        let choosedProds =  JSON.parse(localStorage.getItem("choosedProducts"))
        console.log(choosedProds)
        this.setState({choosedProducts:choosedProds})
    }
    handleAddProduct = (id,product,amount) =>{
        //if user exists, we will see chooses products, choose amount of products and add to added
        //prods , that we will buy later
        if(!localStorage.getItem('user')) {
          return alert("login first")
        }
        let addedProducts = [...this.state.addedProducts];
        for(let i=0; i<amount; i++){
            let products = {id:id, name:product.name, size:product.size,color:product.color, 
              weight:product.weight,description:product.description,price : product.price, 
              shipping : product.shipping,brand:product.brand,src:product.src};
            addedProducts.push(products);
        }
        this.setState({addedProducts:addedProducts},function(){
            console.log(this.state.addedProducts)
        })
        localStorage.setItem("addedProducts", JSON.stringify(addedProducts))
    }
    handleRemoveProduct = (i) => {
        // remove products
            let fullProd = [...this.state.choosedProducts];
            fullProd.splice(i,1);
            this.setState({ choosedProducts : fullProd},()=>{
              localStorage.setItem("choosedProducts", JSON.stringify(this.state.choosedProducts))
            })
    }
    handleBuyProducts = ()=>{
        //buy products
        let zero = 0 ;
        this.state.addedProducts.forEach(element => {
            zero = zero + element.price + element.shipping;
        });
        console.log(this.state.userDatas.balance)
    
        //if user balance is more than zero=sum of product prices
        if(this.state.userDatas.balance > zero) {
          //update boughtproductsin state and ls
            let addedPrd = [...this.state.addedProducts]
            localStorage.setItem("boughtProducts",  JSON.stringify(addedPrd))
            this.setState({boughtProducts : addedPrd}, function () {
            console.log(this.state.boughtProducts)})
    
            //delete addedProducts ls  
            localStorage.removeItem("addedProducts");
            localStorage.removeItem("choosedProducts");
    
            //empty addedProducts
            let copyProducts = [];
            this.setState({addedProducts : copyProducts});
            this.setState({choosedProducts : copyProducts});
            //updating storage user
            let userState = Object.assign({},this.state.userDatas);
            userState.balance = userState.balance - zero;
            this.setState({ userDatas : userState}, function () {
              console.log(this.state.userDatas)})
    
            if(localStorage.getItem("user")) {
                let storageUser =  jwt_decode(localStorage.getItem("user"));
                let copyUser = Object.assign({},storageUser.user);
                copyUser.balance = copyUser.balance - zero;
                let newUser =  jwt.sign({user : copyUser},'jwtPrivateKey');
                localStorage.setItem("user", newUser);
                console.log(jwt_decode(localStorage.getItem("user")))
            }
            let prods ={
                id:this.state.userDatas._id,
                data :addedPrd
              }        
            //add products to bought product collection
            axios.post("http://localhost:3001/boughtProd", prods)
            .then(res => {})
            .catch(error =>console.log(error.response))
              
            // update user balance in database
            axios.post("http://localhost:3001/buyProduct",userState)
            .then(res => console.log(res))
            .catch(error =>console.log(error.response))
            }else {
              return alert("you don't have enough money");
            }
    }  
    componentDidMount = () => {
        this.handleChooseProduct();
         this.handleUserGet();
    }
    render() {
        const {choosedProducts,} = this.state;
        const {handleRemoveProduct,handleBuyProducts} = this;
        
        return(
            <div className='cart'>
                <div className='cart__inner'>
                    <h2 className='cart__title'>Shopping Cart</h2>
                    <div className='cart__header'>
                        <div className='cart__row'>
                            <div className='cart__row--left'>
                                <span className='cart__row-text'>Item</span>
                            </div>
                            <div className='cart__row--right'>
                                <span className='cart__row-text cart__row-text--fixed'>Size</span>
                                <span className='cart__row-text cart__row-text--fixed'>QTY</span>
                                <span className='cart__row-text cart__row-text--fixed'>Price</span>
                                <span className='cart__row-text cart__row-text--fixed'></span>
                                <span className='cart__row-text cart__row-text--fixed'></span>
                            </div>
                        </div>
                    </div>
                    <div className='cart__body'>
                        {choosedProducts ? choosedProducts.map((product,i)=> {
                                return<div className='cart__row' key={i}>
                                        <div className='cart__row--left'>
                                            <div className='cart__body-product'>
                                                <img className='cart__body-product-image' src={product.src[0]} alt='Product'/>
                                                <span className='cart__body-product-title'>
                                                    {product.name}
                                                </span>
                                            </div>
                                        </div>
                                        <div className='cart__row--right'>
                                            <span className='cart__row-text cart__row-text--fixed'>{product.size}</span>
                                            <span className='cart__row-text--fixed' >
                                                <input className='cart__row-text cart__row-input' id={i}/>
                                            </span>
                                            <span className='cart__row-text cart__row-text--fixed'>USD {product.price} </span>
                                            <span className='cart__row-text cart__row-text--fixed'>
                                                <input type='checkbox' onChange={()=> this.handleAddProduct(i, product, document.getElementById(i).value)}/>
                                            </span>
                                            <span className='cart__row-text cart__row-text--fixed' onClick={handleRemoveProduct}>
                                                <i className='icon icon--remove'></i>
                                            </span>
                                        </div>
                                    </div>
                                }
                        ) : <div className='cart__empty'><span>Cart is Empty</span></div>}

                    </div>
                    <div className='cart__footer'>
                        <div className='cart__row'>
                            <div className='cart__row--left'>
                                <button className='cart__row-text-bold cart__row-button' onClick={handleBuyProducts}>BUY PRODUCT</button>                                
                            </div>
                            <div className='cart__row--right'>
                                <div className='cart__footer-text'>
                                    <span className='cart__row-text'>SHIPPING: </span>
                                    <span className='cart__row-text-bold'>USD 15</span>
                                </div>
                                <div className='cart__footer-text'>
                                    <span className='cart__row-text'>TOTAL PRICE: </span>
                                    <span className='cart__row-text-bold cart__row-text-big'>USD 1204</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Cart;