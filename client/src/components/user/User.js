import React from 'react';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import jwt_decode from 'jwt-decode';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import MyProfile from './MyProfile';
import EditUser from './EditProfile';
import AddBalance from './AddBalance';
// import BuyedProduct from '../product/BuyedProduct';

class User extends React.Component {
    state = {
        userDatas : [{
            _id:"",
            username :'',
            email:'',
            password:'',
            age:'',
            birthday:'',
            balance:''
        }],
        choosedProducts : [],
        addedProducts :[],
        prevProducts :[],
        boughtProducts : [],
        userEdit :[],
        }
    handleUserGet = () => {
        //get users if it exists in locastorage
        if(localStorage.getItem('user')){
            const userDatas = jwt_decode(localStorage.getItem('user'));
            const stateCopy = [...this.state.userDatas];
            stateCopy.username = userDatas.user.username;
            stateCopy._id = userDatas.user._id;
            stateCopy.email = userDatas.user.email;
            stateCopy.password = userDatas.user.password;
            stateCopy.age = userDatas.user.age;
            stateCopy.balance = userDatas.user.balance;
            stateCopy.birthday = userDatas.user.birthday;
            this.setState({userDatas:stateCopy})
        } 
    }
    handleEditUser = (event)=>{
        //edit user by user
        event.preventDefault();
        const {username,email,age,newpassword,oldpassword } = event.target;
        const newDatas = {email:email.value,username:username.value,age:age.value,
            newpassword : newpassword.value,oldpassword:oldpassword.value,
            id:this.state.userDatas._id};

        axios.post("http://localhost:3001/editUser" , newDatas)
        .then(token => {
            localStorage.setItem('user', token.data)
            if(localStorage.getItem('user')) {
            let info = jwt_decode(localStorage.getItem('user'));
            const userDatas = Object.assign({}, this.state.userDatas);
            userDatas.username = info.user.username;
            userDatas.email = info.user.email;
            userDatas.password = info.user.password;
            this.setState({userEdit:'You have successfully edited user datas'})
            this.setState({userDatas : userDatas})
            this.handleUserGet()
            }
        })
        .catch(error =>console.log(error.response))
    }
    handleAddBalance = (event)=>{
        // add balance to your account
        event.preventDefault();
        const {bankCard, money} = event.target;
        let cardNum = bankCard.value;
        let moneyAmount = Number(money.value);
        if(moneyAmount < 1) {
          return alert("wrong money amount")
        }
        if(cardNum.length !== 12){
          return alert("wrong credit card number ");
        }
    
       //statis ganaxleba
       if(localStorage.getItem("user")) {
          let userDatasCopy =  Object.assign({}, this.state.userDatas);
                userDatasCopy.balance = userDatasCopy.balance + moneyAmount;
                this.setState({ userDatas : userDatasCopy});
                const user = jwt.sign({user:userDatasCopy},'jwtPrivateKey');
                localStorage.setItem("user", user)
          // bazis ganaxleba
          axios.post("http://localhost:3001/addBalance", userDatasCopy)
          .then(res=> {console.log(res)
          })
          .catch(error => console.log(error.response))
       }
    }
    handleChooseProduct = () =>{
        //if user exists, add choosed products to cart, that we will see in profile page
        if(!localStorage.getItem('user')) { return alert("login first") }
        let choosedProds =  JSON.parse(localStorage.getItem("choosedProducts"))
        this.setState({choosedProducts:choosedProds})
    }
    handleGetBoughtProduct = () => {
        // get previously bought products, by user id
        let id = {id : this.state.userDatas._id};
        console.log(this.state.userDatas)
        axios.post("http://localhost:3001/prevProducts",id)
        .then(res => {
          this.setState({prevProducts : res.data },function(){
            console.log('agiiiiiiiii',this.state.prevProducts)
         
        })
          localStorage.setItem("prevProducts",  JSON.stringify(res.data))
        })
        .catch(error =>console.log(error.response))
    }
    componentDidMount = () => {
        //things to do on home page
        this.handleUserGet();
        this.handleChooseProduct();
        this.handleGetBoughtProduct();
    }
    render() {
        const {userDatas,userEdit,choosedProducts,addedProducts,prevProducts,boughtProducts} = this.state;
        const {handleAddBalance,handleEditUser,handleChooseProduct,handleAddProduct,handleRemoveProduct,handleGetBoughtProduct,handleBuyProducts} = this;
        return(
            <div className='user'>
                <Tabs className='user__tabs'>
                    <TabList className='user__tabs-list'>
                        <Tab className='user__tabs-list__tab'>My Profile</Tab>
                        <Tab className='user__tabs-list__tab'>Edit Profile</Tab>
                        <Tab className='user__tabs-list__tab'>Add Balance</Tab>
                        <Tab className='user__tabs-list__tab' onClick={handleGetBoughtProduct}>Bought Product</Tab>
                    </TabList>

                    <TabPanel className='user__tabs-panel'>
                        <MyProfile userData={userDatas}/>
                    </TabPanel>
                    <TabPanel className='user__tabs-panel'>
                        <EditUser editUser={handleEditUser} editResult={userEdit}/>
                    </TabPanel>
                    <TabPanel className='user__tabs-panel'>
                        <AddBalance addBalance={handleAddBalance}/>
                    </TabPanel>
                    <TabPanel className='user__tabs-panel'>
                        {prevProducts ? prevProducts.map((product,i)=> {
                        let x = product.src.indexOf(',');
                        let y = product.src.slice(0, x);
                        return <div className='cart__row cart__row-history' key={i}>
                                    <div className='cart__row--left'>
                                        <div className='cart__body-product'>
                                            <img className='cart__body-product-image' src={y} alt='Product'/>
                                            <span className='cart__body-product-title'>{product.name}</span>
                                        </div>
                                    </div>
                                    <div className='cart__row--right'>
                                        <span className='cart__row-text cart__row-text--fixed'>{product.size}</span>
                                        <span className='cart__row-text cart__row-text--fixed'>{product.color}</span>
                                        <span className='cart__row-text cart__row-text--fixed'>{product.price}</span>
                                    </div> 
                                    <br></br>
                                </div>
                            }): <span></span>
                        }  
                    </TabPanel>
                </Tabs>
            </div>
        )
    }
}

export default User;