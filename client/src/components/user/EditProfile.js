import React from 'react';

class EditUser extends React.Component {
    render() {
        const { editUser, editResult } = this.props
        return(
            <form className='form' onSubmit={editUser}>
                {editResult ? <p>{editResult}</p> : <p></p>}
                <div className='form__row'>
                    <div className='form__row--left'>
                        <lable className='lable'>Email
                            <input className='form__input' type='text' name="email"/>
                        </lable>
                    </div>
                    <div className='form__row--right'>
                        <lable className='lable'>Username
                            <input className='form__input' type='text' name="username"/>
                        </lable>
                    </div>
                </div>
                <div className='form__row'>
                    <div className='form__row--left'>
                        <lable className='lable'>Age
                            <input className='form__input' type='number' name="age"/>
                        </lable>
                    </div>
                    <div className='form__row--right'></div>
                </div>
                <div className='form__row'>
                    <div className='form__row--left'>
                        <lable className='lable'>Old Password
                            <input className='form__input' type='password' name="oldpassword" required/>
                        </lable>
                    </div>
                    <div className='form__row--right'>
                        <lable className='lable'>New Password
                            <input className='form__input' type='password' name="newpassword" required/>
                        </lable>
                    </div>
                </div>
                <button className='form__button' type="submit">Save Changes </button>
            </form>
        )
    }
}

export default EditUser;