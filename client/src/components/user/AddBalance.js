import React from 'react';

class AddBalance extends React.Component {
    render() {
        const { addBalance } = this.props;
        return(
            <form className='range-slider__container' onSubmit={addBalance}>
                <div className='range-slider__row'>
                    <input className='form__input' type='number' name="bankCard" />
                    <label className='range-slider__lable'>
                        Enter Value Manualy
                        <input className='range-slider__input' type='number' name="money" />
                    </label>
                </div>
                <button className='form__button' type="submit">Add Founds</button>
            </form>
        )
    }
}

export default AddBalance;