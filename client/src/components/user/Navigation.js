import React from 'react';
import {Link} from "@reach/router";

class Navigation extends React.Component {
    render(){
        return(
            <nav>
                <Link to="/home">home</Link>
                <br/>
                <Link to="/registration">registration</Link>
                <br/>
                <Link to="/login">login</Link>
                <br/>
                <Link to="/profile">profile</Link>
                <br/>
                <Link to="/admin">admin</Link>
                <br/>
                <Link to="/adminProfile">admin profile</Link>
                <br/>
                <Link to="/contact">contact page</Link>
            </nav>
        )
    }
}

export default Navigation;