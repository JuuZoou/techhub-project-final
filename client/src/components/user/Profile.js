import React from 'react';

class Profile extends React.Component{
    render(){
        const {logOut,editUser, removeProducts, choosedProducts,
                addProduct,buyProducts,boughtProd,getBoughtProduct, prevProducts,
                userData,addBalance} = this.props;
        return(
            <>
            <div className="main-cont">
                <div>
                    <h3>Profle</h3>
                    id
                    <br />
                    {userData._id}
                    <label>username : </label>
                    <span> {userData.username} </span><br />
                    <label>email : </label>
                    <span> {userData.email} </span><br />
                    <label>balance : </label>
                    <span> {userData.balance} </span><br />
                    <button onClick={logOut}>log out</button>
                    <br />
                    <br />

                    <form onSubmit={addBalance}>
                        <label> add balance </label><br />
                        <label> amoun of money </label>
                        <input name="money" /><br />
                        <label> credit card number</label>
                        <input name="bankCard" /><br />
                        <button >add balance</button><br />
                    </form>
                
                    <br />
                    <br />
                    <form onSubmit={editUser}>
                        <label>edit your account</label>
                        <br/>   
                        username
                        <input type="text" name="username"></input>
                        <br/>
                        email
                        <input type="text" name="email"></input>
                        <br/>
                        passwordrs
                        <input type="text" name="password"></input>
                        <br/>
                        <button type="submit">submit</button>
                    </form>
                </div>

                <div>
                    <h3>added products</h3>
                   
                    {choosedProducts.map((product,i)=> {
                    return <div key={i}>
                          <b>name: {product.name}</b> <br /> 
                          <b> size: {product.size} </b> <br /> 
                          <b> color: {product.color} </b> <br /> 
                          <b> weight: {product.weight} </b> <br /> 
                          <b> description: {product.description} </b> <br /> 
                          <b> price: {product.price} </b> <br /> 
                          <b>amount</b><input type="number" id={i}/> <br />
                          <button onClick={()=>addProduct(i,product.name,product.size,
                          product.color,product.weight,product.description,product.price, 
                            product.shipping,product.brand, document.getElementById(i).value)} >add to list </button> <br />
                          <button onClick={removeProducts}>remove button</button>
                      </div>
                    })
                } 
                    <button onClick={buyProducts}>buy all</button>
                </div>
                
                <div>
                    <b><h3>you have just bought</h3></b>
                    {boughtProd.map((product,i)=> {
                        return <div key={i}>
                                <b>id: {product.id}</b> <br /> 
                                <b>name: {product.name}</b> <br /> 
                                <b> size: {product.size} </b> <br /> 
                                <b>color: {product.color}</b> <br /> 
                                <b>weight: {product.weight}</b> <br /> 
                                <b>description: {product.description}</b> <br /> 
                                <b>price: {product.price}</b> <br />
                                <b>shipping: {product.shipping}</b> <br /> 
                                <b>brand: {product.brand}</b> <br />
                            </div>
                        })} 
                </div>
                <div>
                    <p>previously bought products</p>

            
                    <button onClick={getBoughtProduct}>get previously bought</button>
                    {prevProducts ? prevProducts.map((product,i)=> {
                        return <div key={i}>
                                <b>id: {product.id}</b> <br /> 
                                <b>name: {product.name}</b> <br /> 
                                <b> size: {product.size} </b> <br /> 
                                <b>color: {product.color}</b> <br /> 
                                <b>weight: {product.weight}</b> <br /> 
                                <b>description: {product.description}</b> <br /> 
                                <b>price: {product.price}</b> <br />
                                <b>shipping: {product.shipping}</b> <br /> 
                                <b>brand: {product.brand}</b> <br />
                            </div>
                        }) : <p>no prods yet </p>} 
                    
                </div>        
            </div>  
            </>
        )
    }
}
export default Profile;