import React from 'react';


class MyProfile extends React.Component {
    render() {
        const { userData } = this.props
        return(
            <div className='profile'>
                <div className='profile--left'>
                    <div className='profile__avatar'></div>
                </div>
                <div className='profile--right'>
                    <div className='profile__info'>
                        <span className='profile__info-text'>User ID: <span>{userData._id}</span></span>
                        <span className='profile__info-text'>Username: <span>{userData.username}</span></span>
                        <span className='profile__info-text'>Email: <span>{userData.email}</span></span>
                        <span className='profile__info-text'>Age: <span>{userData.age}</span></span>
                        <span className='profile__info-text'>Balance: <span>{userData.balance} USD</span></span>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyProfile;