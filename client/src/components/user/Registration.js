import React from 'react';

class Registration extends React.Component {
    render(){
        const {regErr,registration } = this.props;

       return(
        <div>
        {regErr ? <p>{regErr}</p> : <p></p>}
        <form onSubmit={registration}>
            <label>username</label>
            <input name="username" required/>
            <br />
            <label>email</label>
            <input name="email"  required />
            <br />
            <label>password</label>
            <input name="password"  required/>
            <br />
            <label>age</label>
            <input type="number" name="age" required/>
            <br />
            <label>birthday</label>
            <input type="date" name="birthday" required/>
            <br />
            <label>balance</label>
            <input  type="number" name="balance" required/>
            <br />
            <button type="submit">submit</button>
        </form>
        </div>
       )
   }
}

export default Registration;
