import React from 'react';

class Home extends React.Component {
    render(){
        const {text,search,priceFilter,handleSizeFilter,brandFilter,getProducts } =this.props;
        return(
            <>
             <div>
                <p>{text} </p>
            </div>
            <form onSubmit={search}>
                <input type="search" name="searchText"/>
                <button>submit</button>
            </form>
            <br />

            <p>price filter</p>
            <select onMouseUp ={priceFilter} >
                <option value="" name="">choose below</option>
                <option value="ascending" name="ascending">ascending</option>
                <option  value="descending" name="descending">descending</option>
            </select>

            <br />
            <form onSubmit={handleSizeFilter}>
                size<input name="size" required /><br />
                <button type="submit">submit</button>
            </form>
            <br />
            <form onSubmit={brandFilter}>
                brand<input name="brand" required/><br />
                <button type="submit">submit</button>
            </form>


            <div>
                {getProducts.map((product,i)=> {
                 return <div key={i}>
                         <b>id: {product.id}</b> <br />
                          <b>name: {product.name}</b> <br /> 
                          <b> size: {product.size} </b> <br /> 
                          <b> color: {product.color} </b> <br /> 
                          <b> weight: {product.weight} </b> <br /> 
                          <b> description: {product.description} </b> <br /> 
                          <b> price: {product.price} </b> <br /> 
                          <b> brand: {product.brand} </b> <br /> 
                          <button onClick={()=>this.props.chooseProducts(product.id,product.name,product.size,
                          product.color,product.weight, product.description,product.price, 
                            product.shipping, product.brand)} >choose product </button> <br />
                      </div>
                    })
                } 
          </div>    
            
          
            </>
        )
    }
}

export default Home;