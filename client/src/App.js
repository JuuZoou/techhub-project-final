// DEFAULT IMPORTS
import React, { Component } from 'react';
import { Router } from "@reach/router";

// COMPONENTS
import Header from './components/header/Header';
import Catalogue from './components/view/catalogue/Catalogue';
import Login from './components/auth/Login';
import SignUp from './components/auth/SignUp';
import User from './components/user/User';
import Cart from './components/user/Cart';
import Contact from'./components/contact/Contact';
import Footer from './components/footer/Footer';
import Admin from './components/admin/Admin';
import AdminProfile from './components/admin/AdminProfile';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header/>
        <main className='content'>
          <Router>
            <Catalogue exact path='/'/>
            <Admin path="/admin"/>
            <AdminProfile path ="/adminProfile"/>
            <Login path='/auth/login'/>
            <SignUp path='/auth/signup'/>
            <User path='/user'/>
            <Cart path='/user/cart' />
            <Contact path='/contact'/>
          </Router>
        </main>
        <Footer/>
      </div>
    );
  }
}

export default App;