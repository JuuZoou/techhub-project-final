const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');

gulp.task('browserSync', () => {
    browserSync.init({
        files: './src/scss/**/*.scss',
        online: true,
        notify: true,
        open: false
    })
});

gulp.task('sass', () => {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/'))
        .pipe(browserSync.stream());
});

gulp.task('watch', () => {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
});

gulp.task('default', ['browserSync','watch']);